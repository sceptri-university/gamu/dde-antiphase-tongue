% Sources of already existing functions
% type dde23
% type ode23

clear
hold on
% Try out (anything and everything) on a barebones toy model at first
% : Parameters of the toy model problem
lags = [2];
hist = [1];
tspan = [0, 4];

% ::: Plotting the analytical solution
tic
times = tspan(1):0.01:tspan(end);
toc
plot(times, arrayfun(@dde_analytical, times))

% ::: Approach using the matlab-native dde23 function
dde_sol = dde23(@dde_system, lags, hist, tspan)
plot(dde_sol.x, dde_sol.y)

% ::: Approach using ChatGPT-generated dde23_to_ode23 function
% Of course it doesn't work...
%gpt_sol = dde23_to_ode23(@dde_system, lags, hist, tspan)
%plot(gpt_sol.x, gpt_sol.y)

% TODO: Implement Method-Of-Steps by hand (or modify the ChatGPT code)

% ::: Different ChatGPT-generated function
tic
const_sol = solveConstantDelayDDE(@dde_system, tspan, 0.01, hist, lags(1), @ode23);
toc
plot(const_sol.x, const_sol.y)

% Define a toy model
function ydot = dde_system(t, y, ydelay)
    ydot = 3*ydelay;
end

function y = dde_analytical(t)
    % This is true only on the 0-3 interval
    if t <= 0
        y = 1;
    elseif t <= 2
        y = 3*t + 1;
    else
        y = 9*t^2 / 2 - 15*t + 19;
    end
end