function step_frequency(signal)::Int64
    signal_step_diference = diff(signal)
    # Find where the signal is incresing, resp. decreasing
    signal_ups = signal_step_diference .> 0
    signal_downs = signal_step_diference .< 0

    # Find where the monotony changes from up to down
    signal_monotony = signal_ups[2:end] .&& signal_downs[1:end-1]

    # Compute mean distance between ups
    ups = findall(identity, signal_monotony)
    avg_distance = mean(diff(ups))

    return round(Int64, avg_distance)
end

function diff_period(; x_mat, kwargs...)
    return step_frequency(view(x_mat, :, 1))
end

function optimal_period(; x_mat, constraints, Δt, opt_method)
    loss(period) = begin
        period = round(Int64, period / Δt)
        return norm(view(x_mat, 1, :) - view(x_mat, period, :))
    end

    result = Optim.optimize(loss, constraints..., opt_method)
    @assert Optim.converged(result) == true "Period convergence not successful, please debug the problem!"
    return round(Int64, result.minimizer / Δt)
end
