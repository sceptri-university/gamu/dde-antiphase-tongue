function sol = dde23_to_ode23(ddefun, lags, history, tspan, options, varargin)
    % Convert DDE problem to an ODE problem and use ode23

    % Define a function for the ODE system
    function dydt = odefunc(t, y)
        Z = lagvals(t, lags, history, t, y, [], varargin{:});
        dydt = feval(ddefun, t, y, Z, varargin{:});
    end

    % Check inputs
    if nargin < 5
        options = [];
        if nargin < 4
            error('Not enough input arguments');
        end
    end

    % Initialize time span and history
    t0 = tspan(1);
    tfinal = tspan(end);
    if tfinal <= t0
        error('Invalid time span');
    end

    % Create an initial condition for the ODE
    Z0 = lagvals(t0, lags, history, t0, history, [], varargin{:});
    y0 = feval(ddefun, t0, history, Z0, varargin{:});

    % Use ode23 to solve the ODE problem
    [t_ode, y_ode] = ode23(@odefunc, tspan, y0, options);

    % Create the solution structure
    sol = struct('x', t_ode, 'y', y_ode, 'solver', 'ode23');

    % Note: The 'discont' and event handling parts are not included in this conversion
end

function Z = lagvals(tnow,lags,history,X,Y,YP,varargin)
% For each I, Z(:,I) is the solution corresponding to TNOW - LAGS(I).
% This solution can be computed in several ways: the initial history,
% interpolation of the computed solution, extrapolation of the computed
% solution, interpolation of the computed solution plus the tentative
% solution at the end of the current step.  The various ways are set
% in the calling program when X,Y,YP are formed.

% No lags corresponds to an ODE.
if isempty(lags)
  Z = [];
  return;
end

% Typically there are few lags, so it is reasonable to process 
% them one at a time.  NOTE that the lags may not be ordered and 
% that it is necessary to preserve their order in Z.
xint = tnow - lags;
Nxint = length(xint);
if isstruct(history)
  given_history = history.history;
  tstart = history.x(1);
  neq = length(history.y(:,1));
else
  neq = length(Y(:,1));
end

if isnumeric(history)
  Z = zeros(neq,Nxint,class(history));
else
  Z = zeros(neq,Nxint,class(Y));
end

for j = 1:Nxint
  if xint(j) < X(1)
    if isnumeric(history)
      temp = history;
    elseif isstruct(history)
      % Is xint(j) in the given history?          
      if xint(j) < tstart
        if isnumeric(given_history)
          temp = given_history;
        else
          temp = feval(given_history,xint(j),varargin{:});
        end
      else    
        % Evaluate computed history by interpolation. Mute unwanted warning.
        ws = warning('off','MATLAB:deval:NonuniqueSolution');
        temp = deval(history,xint(j));
        warning(ws);
      end
    else
      temp = feval(history,xint(j),varargin{:});
    end
    Z(:,j) = temp(:); 
  else
    % Find n for which X(n) <= xint(j) <= X(n+1).  xint(j) bigger
    % than X(end) are evaluated by extrapolation, so n = end-1 then.
    indices = find(xint(j) >= X(1:end-1));
    n = indices(end);
    Z(:,j) = ntrp3h(xint(j),X(n),Y(:,n),X(n+1),Y(:,n+1),YP(:,n),YP(:,n+1));
  end 
end
end