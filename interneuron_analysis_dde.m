clear
profile on

f_sol = figure();
hold on
% : Simulation parameters
delta_t=0.05;
tau=0.01;
tspan = [0, 1000];

% Precomputing values for easier handling later
interneuron_parameters = interneuron_params();
dde_system = @(t,x,z) interneuron(x, z, interneuron_parameters, t);

tic
dde_sol = dde23(dde_system, [tau], interneuron_history(), tspan);
toc
plot(dde_sol.x, dde_sol.y(1,:))

tic
ode23_sol = solveConstantDelayDDE(dde_system, tspan, delta_t, interneuron_history(), tau, @ode23);
toc
plot(ode23_sol.x, ode23_sol.y(1,:))

tic
ode45_sol = solveConstantDelayDDE(dde_system, tspan, delta_t, interneuron_history(), tau, @ode45);
toc
plot(ode45_sol.x, ode45_sol.y(1,:))

f_scatter = figure();
step_diff = diff(dde_sol.x);
window_size = 1;
window_steps = int32(window_size / min(step_diff));


scatter(dde_sol.x(2:end), step_diff)
%plot(dde_sol.x(2:end), cumsum(step_diff) / tspan(end))

f_steps = figure();
hold on
%plot(ode23_sol.x, ode23_sol.ode_steps / 250)
plot(dde_sol.x(2:end), movmean(movmedian(step_diff, window_steps), window_steps)) 

f_comp = figure();

min_sol = min(dde_sol.y(1, :));
max_sol = max(dde_sol.y(1, :));
stt_sol = (dde_sol.y(1,:) - min_sol) / (max_sol - min_sol);
stt_sol_diff = diff(stt_sol);
abs_sol_diff = abs(stt_sol_diff);

ratio_diff = abs_sol_diff ./ step_diff;
%plot(dde_sol.x(2:end), ratio_diff)
plot(dde_sol.x(2:end), movmean(ratio_diff, window_steps))

function hist = interneuron_history(p, t)
    arguments
        p (1,11) double = interneuron_params()
        t (1,1) double = 0
    end
    
    hist = [-16.34, 0.06, 0.55, -33.4, 0.49, 0.4];
end

function params = interneuron_params(coupling, C1, tau)
    arguments
        coupling (1,1) double = 0
        C1 (1,1) double = 0.98
        tau (1,1) double = 0.01
    end
    
    params = [C1, 1, 24, 0.1, -60, 30, 45, 20, -80, coupling, tau];
end