function dydt=ddefun(t,y,Z,par)

lag1=Z(:,1);

dydt=sys_rhs([y lag1],par);