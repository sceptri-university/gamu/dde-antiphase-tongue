function dx = interneuron(x, h, p, t)
    arguments
        x (1,6) double
        h (1,6) double
        p (1,11) double
        t (1,1) double
    end
    
    % Decomposing arguments
    V1=x(1);h1=x(2);n1=x(3);
    V2=x(4);h2=x(5);n2=x(6);
    
    Vh1=h(1);Vh2=h(4);
    
    C1=p(1);C2=p(2);I=p(3);
    gL=p(4);EL=p(5);
    gNa=p(6);ENa=p(7);
    gK=p(8);EK=p(9);
    couple=p(10);tau=p(11);
    
    dx=zeros(6,1);
    dx(1,1) = 1 / C1 * (I - gL * (V1 - EL) - gNa / (1 + exp(-.8e-1 * V1 - 2.08))^3 * h1 * (V1 - ENa) - gK * n1^4 * (V1 - EK) - couple * (V1 - Vh2));
    dx(2,1) = 1.666666667 * (1 / (1 + exp(0.13 * V1 + 4.94)) - h1) * (1 + exp(-0.12 * V1 - 8.04));
    dx(3,1) = (1 / (1 + exp(-.45e-1 * V1 - 0.450)) - n1) / (0.5 + 2 / (1 + exp(.45e-1 * V1 - 2.250)));
    dx(4,1) = 1 / C2 * (I - gL * (V2 - EL) - gNa / (1 + exp(-.8e-1 * V2 - 2.08))^3 * h2 * (V2 - ENa) - gK * n2^4 * (V2 - EK) - couple * (V2 - Vh1));
    dx(5,1) = 1.666666667 * (1 / (1 + exp(0.13 * V2 + 4.94)) - h2) * (1 + exp(-0.12 * V2 - 8.04));
    dx(6,1) = (1 / (1 + exp(-.45e-1 * V2 - 0.450)) - n2) / (0.5 + 2 / (1 + exp(.45e-1 * V2 - 2.250)));
end