norm_metric(A,B) = norm(A - B)
max_metric(A,B) = maximum(abs.(A - B))

function period_shift(; x_mat, constraints, Δt, period::Integer, num_periods::Integer, opt_method, metric, kwargs...)
    x_length = size(x_mat)[1]

    loss(shift) = begin
        shift = round(Int64, shift / Δt)

        V1 = view(x_mat, (x_length - num_periods * period) : x_length, 1)
        V2 = view(x_mat, (x_length - num_periods * period - shift) : (x_length - shift), 4)

        return metric(V1, V2)
    end

    shift_constraints = @. period * Δt * constraints
    result = Optim.optimize(loss, shift_constraints..., opt_method)
    # we want to return the actual constraint
    @assert Optim.converged(result) == true "Shift convergence not successful, please debug the problem!"
    return result.minimizer
end

function partial_shift(; x_mat, constraints, Δt, period::Integer, partial_trajectory::AbstractFloat, opt_method, metric, kwargs...)
	@assert 0 <= partial_trajectory <= 1 "partial_trajectory dictates how much of found trajectory use for shift searching, hence it must lie between 0 and 1!"

    x_length = size(x_mat)[1]
	take_length = round(Int64, x_length * partial_trajectory)

    loss(shift) = begin
        shift = round(Int64, shift / Δt)

        V1 = view(x_mat, (x_length - take_length) : x_length, 1)
        V2 = view(x_mat, (x_length - take_length - shift) : (x_length - shift), 4)

        return metric(V1, V2)
    end

    shift_constraints = @. period * Δt * constraints
    result = Optim.optimize(loss, shift_constraints..., opt_method)
    # we want to return the actual constraint
    @assert Optim.converged(result) == true "Shift convergence not successful, please debug the problem!"
    return result.minimizer
end