# Phase/Anti-phase Tongue Generation in 2 Coupled Delayed Interneuron Models

This project serves to generate phase/anti-phase tongue in coupled delayed interneuron models *and to do it fast*. The key file here is `tongue.jl` where `generate_tongue` function is called.

## Important concepts

### Iterators (Enumerators)

Iterators determine the way our method steps through the whole parameter/delay space in which we want to calculate the tongue. Namely, we have implemented 

- `line_enumerator` which enumerates the space in rows from the bottom left iterating through the row and up,
- `spiral_enumerator` which enumerates the space in a spiral from the "center" -- it can be used in a multi-threaded setup to speed up the computation as it allows effective reusing of nearby histories (= already computed results) such that we shouldn't fall off of the attractor too soon.

### Starters & Histories
> Although there are two history types, only `NaiveHistory` is fully implemented.

`NaiveHistory` assumes that for a new point being calculated, the history is taken as the last point of the trajectory of a nearby point selected by the starter.

Starters, on the other hand, determine which (if any) already calculated result is used for history. There are 3 types of starters

- `cold_start` - always starts the solving of the DDE problem with the initial guess
- `warm_start` - searches the whole already-computed space and finds such point (and its history), such that it minimizes some loss function
- `indexed_start` - searches only indices given by the *indexer* to find the minimizer point of a given loss function

Typically a diagonal indexer of length 5 is used with `indexed_start` and the loss function minimized is
$$
\mathrm{loss}(x_{i_1, \dots, i_n}, y_{j_1, \dots, j_n}) = ||(i_1, \dots, i_n) - (j_1, \dots, j_n)|| \cdot (|x_{i_1, \dots, i_n} - y_{j_1, \dots, j_n} - 0.5| + 0.5)
$$

### Period searching

Given a simulated trajectory by the DDE solving algorithm, we try to estimate the length of its period. Again, there are 2 methods:

- `optimal_period` - the optimal shift such that the norm between the shifted and original trajectory is minimal
- `diff_period` - the average distance between points when the $V_1$ part of the trajectory changes monotony

### Shift searching

Shift searching methods strive to find the shift between $V_1$ and $V_2$ parts of the simulated trajectories. One can use the entire trajectory to try to find this shift by minimizing a metric of the difference between one shifted and one left alone -- this is called the `period_shift`. The second option is to use only a part of this trajectory, which is implemented via `partial_shift`.

The metric can either be a norm or a maximal metric of the absolute value of the difference.

## Tongue generation

By choosing an enumerator, history, starter (and possibly also an indexer) along with a period searching algorithm and finally a metric and a shift searching algorithm, one can run the function `generate_tongue`, which computes the shift-tongue given some parameter-delay range.

A multithreaded run is also possible, using `ReentrantLock`s. I would only advise it to be used with `diagonal_indexer` in the case of `indexed_starting` (or with a `cold_start`), as in other circumstances, its benefits would be marginal (or none at all).

## License

All of the Julia code is licensed under MIT license. Most (but not all) of the Matlab code is the property of Jakub Záthurecký -- although it is likely that also the Matlab code shares this license until explicitly confirmed I shall make no such claim (and you will have to contact the author himself in case you want to use it).