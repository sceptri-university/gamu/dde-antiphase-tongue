line_iterator(collection1, collection2) = ([c1, c2] for c1 in collection1 for c2 in collection2)
line_enumerator(collection1, collection2) = (([i1, i2], [c1, c2])
                                             for (i1, c1) in enumerate(collection1)
                                             for (i2, c2) in enumerate(collection2)
)

Base.@kwdef mutable struct SpiralCollection{T<:AbstractArray}
    horizontal_array::T
    vertical_array::T
    begins_horizontal::Bool = false
    enumeration::Bool = false
end

Base.length(spiral::SpiralCollection) = length(spiral.horizontal_array) * length(spiral.vertical_array)

function Base.iterate(spiral::SpiralCollection)
    h_center, v_center = center(spiral)
    h_item = spiral.horizontal_array[h_center]
    v_item = spiral.vertical_array[v_center]

    output = spiral.enumeration ? ((h_center, v_center), [h_item, v_item]) : [h_item, v_item]

    return (output, (h_center, v_center, 0, 1, spiral.begins_horizontal, 1, 1))
end

function Base.iterate(spiral::SpiralCollection, state)
    horizontal_index, vertical_index, made_steps, max_steps,
    move_horizontal, direction, seen = state

    if seen >= length(spiral)
        return nothing
    end

    new_horizontal = move_horizontal ? horizontal_index - direction : horizontal_index
    new_vertical = !move_horizontal ? vertical_index - direction : vertical_index

    all_in_bounds = inbounds(new_vertical, spiral.vertical_array) &&
                    inbounds(new_horizontal, spiral.horizontal_array)

    if all_in_bounds
        horizontal_item = spiral.horizontal_array[new_horizontal]
        vertical_item = spiral.vertical_array[new_vertical]

        seen += 1
    end

    made_steps += 1

    if made_steps >= max_steps
        # if we finished the full rotation, start again with longer max_steps
        if spiral.begins_horizontal != move_horizontal
            direction = -direction
            max_steps += 1
        end

        move_horizontal = !move_horizontal
        made_steps = 0
    end

    if !all_in_bounds
        @debug "out of bounds from $horizontal_index, $vertical_index to $new_horizontal, $new_vertical"
        return iterate(spiral, (new_horizontal, new_vertical, made_steps,
            max_steps, move_horizontal, direction, seen))
    end

    output = spiral.enumeration ?
             ((new_horizontal, new_vertical), [horizontal_item, vertical_item]) :
             [horizontal_item, vertical_item]

    return (output, (new_horizontal, new_vertical, made_steps, max_steps, move_horizontal, direction, seen))
end

function Base.enumerate(spiral::SpiralCollection)
    spiral.enumeration = true
    return spiral
end

center(spiral::SpiralCollection) = [length(spiral.horizontal_array) + 1, length(spiral.vertical_array) + 1] .÷ 2

inbounds(index, collection) = 0 < index <= length(collection)

spiral_iterator(collection1, collection2; begins_horizontal=false) = SpiralCollection(collection1, collection2, begins_horizontal, false)
spiral_enumerator(collection1, collection2; begins_horizontal=false) = SpiralCollection(collection1, collection2, begins_horizontal, true)