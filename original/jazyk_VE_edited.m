function [A]=jazyk(m,n)

diff=zeros(1,100);
A=zeros(m,n);
x=linspace(500,1000,15000);

for k=linspace(0,0.04,m)
    for l=linspace(0.98,1.02,n)
        sol=dde23(@(t,y,Z) ddefun(t,y,Z,[l,1,24,0.1,-60,30,45,20,-80,k,0.001]),[0.01],@(t) [-16.34;0.06;0.55;-33.4;0.49;0.4],[0 1000]);
        
        u=deval(sol,x);
        for i=1:100
            diff(i)=sum((u(1,14900:15000)-u(1,(14900-i):(15000-i))).^2);
        end
        [M,pp]=min(diff);
        
        diff2=zeros(1,round(pp/2+pp*0.1)-round(pp/2-pp*0.1)+1);
        for i=round(pp/2-pp*0.1):round(pp/2+pp*0.1)
            diff2(i-round(pp/2-pp*0.1)+1)=max(abs(u(1,150:end)-u(4,(150-i):(end-i))));
        end
        A(round((m-1)/0.04*k+1),round((n-1)/0.04*l+1-24.5*(n-1)))=min(diff2);
    end
    display(['line ',num2str(k)])
end

A=A(end:-1:1,:);
imshow(A,[min(min(A)),max(max(A))])
save('matica51x51_stotina.mat','A')