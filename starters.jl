using LinearAlgebra

function warm_start(initial_guess; loss=multiplicative_loss, kwargs...)
    return (coords, histories, diffs) -> begin
        unknown_limits = isnothing.(histories)
        if all(unknown_limits)
            return initial_guess
        end

        limit_indices = [[ia, ib] for
                         ia in eachindex(eachrow(histories)),
                         ib in eachindex(eachcol(histories))
                         if !unknown_limits[ia, ib]
        ]

        _, closest_index = findmin(loss(known_coords .- coords, diffs[known_coords...]) for known_coords in limit_indices)
        closest_coords = limit_indices[closest_index]

        return histories[closest_coords...]
    end
end

multiplicative_loss(coords_diff, difference) = norm(coords_diff) * (abs(difference - 0.5) + 0.5)

"""
A more liberal and error-prone, but a constant-time warm start
"""
function indexed_start(initial_guess; loss=multiplicative_loss, grid_size::Tuple{TI,TI}, indexer=diagonal_indexer, indexer_N, kwargs...) where TI <: Integer
	return (coords, histories, diffs) -> begin
		go_to_direction = directions(coords, grid_size)

        indices = indexer(coords, go_to_direction, grid_size; N=indexer_N)
        known_indices = filter(coords -> !isnothing(histories[coords...]), indices)
		
        if isempty(known_indices)
            @info "Using the initial guess for $coords with direction $go_to_direction on thread #" * string(Threads.threadid())
            return initial_guess
        end

        _, best_index = findmin(loss(known_coords .- coords, diffs[known_coords...]) for known_coords in known_indices)
        best_coords = known_indices[best_index]
		
        return histories[best_coords...]
	end
end

function diagonal_indexer(coords::Tuple{TI,TI}, directions::Tuple{TJ,TJ}, grid_size::Tuple{TK,TK}; N=5) where {TI<:Integer,TJ<:Integer, TK<:Integer}
	# Consider indices upto N-th row/column away in the direction of the center
	#	3333
	#	2223
	#	1123
	#	x123
	indices = Vector{Tuple{TI,TI}}(undef, N*(N+2))
	write_index = 0

	inbounds_write(at, write_to, subject, sizes) = begin
		if !inbounds(subject, sizes)
			return at
		end

		at += 1
		write_to[at] = subject

		return at
	end

	for n in 1:N
		# Move the the initial position for each distance
        index_pair = coords .+ n .* (0, directions[2])

		write_index = inbounds_write(write_index, indices, index_pair, grid_size)

		# And then iterate the entire L shape
		for i in 2:(2*n+1)
			new_directions = i > n ? (0, -directions[2]) : (directions[1], 0)
            index_pair = index_pair .+ new_directions

            write_index = inbounds_write(write_index, indices, index_pair, grid_size)
		end
	end

	return view(indices, 1:write_index)
end

inbounds(index_pair::Tuple{TI,TI}, collection_size::Tuple{TJ,TJ}) where {TI<:Integer,TJ<:Integer} = (0 < index_pair[1] <= collection_size[1]) && (0 < index_pair[2] <= collection_size[2])

"""
Directions from the current coordinates to the center.

> Note: *The calculation should mimic the center calc of spiral iterator*
"""
directions(coords, grid_size)::Tuple{Int32, Int32} = 2 .* (coords[1] < ((grid_size[1] + 1) ÷ 2), coords[2] < ((grid_size[2] + 1) ÷ 2)) .- 1

cold_start(initial_guess; kwargs...) = (args...; kwargs...) -> initial_guess