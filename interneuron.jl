function interneuron(dx, x, h, p, t)
    V1, h1, n1, V2, h2, n2 = x
    C1, C2, I, gL, EL, gNa, ENa, gK, EK, couple, τ = p
    Vh1, _, _, Vh2, _, _ = h(p, t - τ)

    dx[1] = 1 / C1 * (I - gL * (V1 - EL) - gNa / (1 + exp(-.8e-1 * V1 - 2.08))^3 * h1 * (V1 - ENa) - gK * n1^4 * (V1 - EK) - couple * (V1 - Vh2))
    dx[2] = 1.666666667 * (1 / (1 + exp(0.13 * V1 + 4.94)) - h1) * (1 + exp(-0.12 * V1 - 8.04))
    dx[3] = (1 / (1 + exp(-.45e-1 * V1 - 0.450)) - n1) / (0.5 + 2 / (1 + exp(.45e-1 * V1 - 2.250)))
	
    dx[4] = 1 / C2 * (I - gL * (V2 - EL) - gNa / (1 + exp(-.8e-1 * V2 - 2.08))^3 * h2 * (V2 - ENa) - gK * n2^4 * (V2 - EK) - couple * (V2 - Vh1))
    dx[5] = 1.666666667 * (1 / (1 + exp(0.13 * V2 + 4.94)) - h2) * (1 + exp(-0.12 * V2 - 8.04))
    dx[6] = (1 / (1 + exp(-.45e-1 * V2 - 0.450)) - n2) / (0.5 + 2 / (1 + exp(.45e-1 * V2 - 2.250)))
end

interneuron_params(; coupling=0, C1=0.98, τ=0.01) =
    (C1, 1, 24, 0.1, -60, 30, 45, 20, -80, coupling, τ)

interneuron_history(p, t; initial_point=[-16.34, 0.06, 0.55, -33.4, 0.49, 0.4]) = initial_point
interneuron_history(; initial_point) = (p, t) -> interneuron_history(p, t; initial_point)
