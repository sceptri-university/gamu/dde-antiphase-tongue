abstract type DDEHistory end

type(history::DDEHistory) = Union{Nothing, eltype(history)}

Base.@kwdef struct NaiveHistory <: DDEHistory
	dim::Int64
end
naive_history(; dim, kwargs...) = NaiveHistory(; dim)

"""
N-step history is a generalization of the naive history (naive history is 1-step history)

**WARNING**: It is **NOT** finished (nor is it fully implemented in tongue.jl)
"""
Base.@kwdef struct NstepHistory <: DDEHistory
	dim::Int64
	n::Int64
end
nstep_history(; dim, n, kwargs...) = NstepHistory(; dim, n)

subtype(::DDEHistory) = Nothing
eltype(history::NaiveHistory) = Tuple{Type[Float64 for _ in 1:history.dim]...}

subtype(history::NstepHistory) = Tuple{Type[Float64 for _ in 1:history.dim]...}
eltype(history::NstepHistory) = Tuple{Type[subtype(history) for _ in 1:history.n]...}

final(::NaiveHistory, history_instance) = collect(history_instance)
final(::NstepHistory, history_instance) = collect(history_instance[end])

get(::NaiveHistory, point) = (p, t) -> point
get(history::NstepHistory, sequence) = (p, t) -> begin
    @show p, t, sequence
    return final(history, sequence)
end

convert(history::NaiveHistory, point) = eltype(history)(point)
convert(history::NstepHistory, sequence) = eltype(history)(subtype(history)(point) for point in sequence)