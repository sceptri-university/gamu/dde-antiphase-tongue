using DifferentialEquations, LinearAlgebra, Optim
using Plots
using SharedArrays, Base.Threads
using Profile
using Serialization

gr()
#plotlyjs()

to_matrix(trajectory) = vcat(reshape.(trajectory, 1, length(trajectory[end]))...)

include("interneuron.jl")
include("period_searching.jl")
include("shift_searching.jl")
include("iterators.jl")
include("histories.jl")
include("starters.jl")

function calculate_tongue_pixel(indices, values,
    multithreading_lock, use_locks,
    starter, history,
    histories, diffs,
    trajectories, save_all_trajectories, save_individually,
    lags, tspan, num_periods,
    alg, Δt, τ,
    period_alg, opt_method,
    period_constraints, shift_constraints,
    find_shift, partial_trajectory,
    metric,
    kwargs...)

    coupling_index, C1_index = indices
    coupling, C1 = values
    # TODO: iterate from both sides and keep maximum
    # TODO: implementent stitching as an approximation of the tounge-curve - when too wide, split it
    # TODO: compute variance of shift computed with varying timespan

    if use_locks
        lock(multithreading_lock) do
            global selected_history = starter(indices, histories, diffs)
        end
    else
        global selected_history = starter(indices, histories, diffs)
    end

    problem = DDEProblem(interneuron, final(history, selected_history), get(history, selected_history),
        tspan, interneuron_params(; coupling, C1, τ);
        constant_lags=lags
    )

    sol = solve(problem, alg, saveat=Δt)
    if sol.retcode != SciMLBase.ReturnCode.Success
        @warn "Solve not successful for C₁ = $C1, coupling = $coupling"
        return
    end
    sol_length = length(sol.u)

    x = view(sol.u, (sol_length÷2):sol_length)
    x_mat = to_matrix(x)

    period = period_alg(; x_mat, Δt, opt_method, constraints=period_constraints)

    if use_locks
        lock(multithreading_lock) do
            histories[coupling_index, C1_index] = convert(history, x[end])
            diffs[coupling_index, C1_index] = find_shift(; x_mat, constraints=shift_constraints, Δt,
                period, num_periods, opt_method, partial_trajectory, metric) / (period * Δt)

            if save_individually
                metric_name = nameof(metric) |> string
                serialize("./trajectories/{$metric_name}_C1={$C1}_coupling={$coupling}.dat", x_mat)
            elseif save_all_trajectories
                trajectories[coupling_index, C1_index] = x_mat
            end
        end
    else
        histories[coupling_index, C1_index] = convert(history, x[end])
        diffs[coupling_index, C1_index] = find_shift(; x_mat, constraints=shift_constraints, Δt,
            period, num_periods, opt_method, partial_trajectory, metric) / (period * Δt)

        if save_individually
            metric_name = nameof(metric) |> string
            serialize("./trajectories/{$metric_name}_C1={$C1}_coupling={$coupling}.dat", x_mat)
        elseif save_all_trajectories
            trajectories[coupling_index, C1_index] = x_mat
        end
    end
end

function generate_tongue(length_coupling, length_C1;
    max_C1=1.05,
    min_C1=0.95,
    max_coupling=0.06,
    min_coupling=0.0,
    Δt=0.05,
    τ=0.01,
    tspan=(0.0, 1000.0),
    x0=[-16.34, 0.06, 0.55, -33.4, 0.49, 0.4],
    # period finding
    period_alg=diff_period,
    period_constraints=(2, 4),
    # general optimization
    opt_method=Optim.GoldenSection(),
    # shift finding (using optimization)
    shift_constraints=(0, 0.95),
    num_periods=10,
    enumerator=line_enumerator,
    starter=cold_start,
    history=naive_history,
    use_locks=false,
    save_all_trajectories=false,
    save_individually=false,
    multithreading_lock=ReentrantLock(),
    find_shift=partial_shift,
    partial_trajectory=0.5,
    metric=norm_metric,
    kwargs...
)
    alg = MethodOfSteps(Tsit5())

    lags = [τ]
    diffs = Matrix{Float64}(undef, length_coupling, length_C1)
    trajectories = save_all_trajectories ? Matrix{Matrix{Float64}}(undef, length_coupling, length_C1) : nothing

    history = history(; dim=length(x0), n=max(1, floor(Int64, τ / Δt)))
    histories = type(history)[nothing for _ in 1:length_coupling, _ in 1:length_C1]

    if starter != cold_start && nthreads() > 1 && !use_locks
        @warn "Usage of warm starters in a multithreaded environment without locks is dangerous"
    end

    coupling_collection = range(min_coupling; stop=max_coupling, length=length_coupling)
    C1_collection = range(min_C1; stop=max_C1, length=length_C1)

    starter = starter(x0; grid_size=(length_coupling, length_C1), kwargs...)
    parameter_collection = collect(enumerator(coupling_collection, C1_collection))

    @sync @threads :static for thread in 1:nthreads()
        for (indices, values) in view(parameter_collection, thread:nthreads():(length_C1*length_coupling))
            calculate_tongue_pixel(indices, values,
                multithreading_lock, use_locks,
                starter, history,
                histories, diffs,
                trajectories, save_all_trajectories, save_individually,
                lags, tspan, num_periods,
                alg, Δt, τ,
                period_alg, opt_method,
                period_constraints, shift_constraints,
                find_shift, partial_trajectory,
                metric,
                kwargs...)
        end
    end

    return diffs, trajectories
end

couplings = 50
C1s = 50

C1_collection = range(0.95; stop=1.05, length=C1s)
coupling_collection = range(0.0; stop=0.06, length=couplings)

println("optimized period")
# Works kinda weird...
# @time diffs_o_spiral = generate_tongue(couplings, C1s; Δt=0.025, period_alg=optimal_period, enumerator=spiral_enumerator, starter=warm_start);
# @time diffs_o_line = generate_tongue(couplings, C1s; Δt=0.025, period_alg=optimal_period, enumerator=line_enumerator, starter=warm_start);
# diffs_o = max.(abs.(diffs_o_line .- 0.5), abs.(diffs_o_spiral .- 0.5))
@time diffs_o_max, _ = generate_tongue(couplings, C1s;
    Δt=0.025,
    τ=0.03,
    tspan=(0.0, 1000.0),
    period_alg=optimal_period,
    enumerator=spiral_enumerator,
    starter=indexed_start,
    indexer_N=10,
    use_locks=true,
    save_all_trajectories=false,
    partial_trajectory=0.7,
    save_individually=true,
    metric=max_metric
);

@time diffs_o, trajectories_o = generate_tongue(couplings, C1s;
    Δt=0.025,
    τ=0.03,
    tspan=(0.0, 1000.0),
    period_alg=optimal_period,
    enumerator=spiral_enumerator,
    starter=indexed_start,
    indexer_N=10,
    use_locks=true,
    save_all_trajectories=false,
    partial_trajectory=0.7,
    save_individually=true,
    metric=norm_metric
);

serialize("opt_diff_naive_indexed_spiral_650_tspan_1000_shift_from_0_7_max_metric.dat", diffs_o_max)

@time diffs_o_warm, _ = generate_tongue(couplings, C1s;
    Δt=0.025,
    τ=0.03,
    period_alg=optimal_period,
    enumerator=spiral_enumerator,
    starter=warm_start
);

@time diffs_o_cold, _ = generate_tongue(couplings, C1s;
    Δt=0.025,
    τ=0.03,
    period_alg=optimal_period,
    enumerator=spiral_enumerator,
    starter=cold_start
);

println("diff period")
@time diffs_p, _ = generate_tongue(couplings, C1s;
    Δt=0.025,
    period_alg=diff_period,
    enumerator=spiral_enumerator,
    starter=warm_start
);

ho = heatmap(
    C1_collection,
    coupling_collection,
    diffs_o;
    xlabel="C₁",
    ylabel="coupling ε",
    title="Spiral warm start",
    c=:romaO
)

po = plot(trajectories_o[18, 26][4*end÷5:end, 1], trajectories_o[18, 26][4*end÷5:end, 4])
savefig(po, "18_26_opt_diff_naive_spiral_indexed_mt_1000.png")

hp = heatmap(
    C1_collection,
    coupling_collection,
    diffs_p;
    xlabel="C₁",
    ylabel="coupling ε",
    title="Diff warm spiral",
    c=:romaO
)
ho_cold = heatmap(
    C1_collection,
    coupling_collection,
    diffs_o_cold;
    xlabel="C₁",
    ylabel="coupling ε",
    title="Spiral cold start",
    c=:romaO
)
ho_warm_cold = plot(ho, ho_cold, layout=2)
hop = plot(ho, hp, layout=2)
savefig(ho_warm_cold, "opt_diff_shift_cold_warm_spiral.png")
savefig(ho, "opt_diff_spiral_naive_history_indexed_start_mt_650_tspan_1000_shift_from_07_max_metric.png")


iteration_order = [0 for c in C1_collection, cc in coupling_collection]
i = 1
for (coords, _) in spiral_enumerator(C1_collection, coupling_collection)
    iteration_order[coords...] = i
    i += 1
end

hp = heatmap(
    range(0.98; stop=1.02, length=C1s),
    range(0.0; stop=0.04, length=couplings),
    iteration_order
)


hd = heatmap(diffs_o .- diffs_p, title="final shift difference")